#ifndef __LINUX_GETCH_H__
#define __LINUX_GETCH_H__

#include <fstream>
#include "key.h"

namespace Getkey{
    bool is_input_from_file(int argc, char** argv); //returns true if input is from a file
    Key getkey(); //returns the key pressed
    int getchar(Key key);
    void waitForKey(Key key = ANY);
}

#endif